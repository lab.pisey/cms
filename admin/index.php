<?php
    session_start();
    include_once('../include/connection.php');
    if(isset($_SESSION['logged_in'])){
        ?>
            <html>
                <head><title>
                Admin Page
                </title>
                <link rel="stylesheet" href="../assets/style.css"/>
                </head>
                <body>
                    <div class="container">
                        <a href="../index.php" id="logo">CMS</a>
                        <br/>
                        <ol>
                            <li><a href="add.php">Add Article</a></li>
                            <li><a href="delete.php">Delete Article</a></li>
                            <li><a href="../index.php">All Articles</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ol>
                            
                    </div>
                </body>
            </html>
        <?php
    }else{
        if(isset($_POST['username'],$_POST['password'])){
            $username = $_POST['username'];
            $password = md5($_POST['password']);

            if(empty($username) or empty($password)){
                $error = 'All fields are required!';
            }else{
                $query = $pdo->prepare("SELECT * FROM users WHERE user_name = ? and user_password = ? ");
                $query -> bindValue(1,$username);
                $query ->bindValue(2,$password);
                $query->execute();

                $num = $query->rowCount();
                if($num==1){
                    $_SESSION['logged_in'] = true;
                    header("Location: index.php");
                    exit();
                }else{
                    $error = 'Incorrect details!';
                }
            }
        }
        ?>
            <html>
                <head><title>
                Login Page
                </title>
                <link rel="stylesheet" href="../assets/style.css"/>
                </head>
                <body>
                    <div class="container">
                        <a href="../index.php" id="logo">CMS</a>
                        <br/><br/>
                            <?php 
                                if(isset($error)){ ?>
                                    <small style="color:#aa0000;"><?php echo $error; ?> </small>
                            <?php  }  ?>
                            <br/><br/>
                        <form action="index.php" method="post">
                            <input type="text" name="username" placeholder="Username"/>
                            <input type="password" name="password" placeholder="Password"/>
                            <input type="submit" value="Login" />
                        </form>
                    </div>
                </body>
            </html>
        <?php
    }
?>