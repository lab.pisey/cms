<?php
    session_start();
    include_once('../include/connection.php');
    if(isset($_SESSION['logged_in'])){
        if(isset($_POST['title'],$_POST['content'])){
            $title = $_POST['title'];
            $content = nl2br($_POST['content']);

            if(empty($title) or empty($content)){
                $error = 'All fields are required!';
            }else{
                $query = $pdo->prepare("INSERT INTO articles(article_title,article_content,article_timestamp) VALUES(?,?,?)");
                $query->bindValue(1,$title);
                $query->bindValue(2,$content);
                $query->bindValue(3,time());

                $query->execute();
                header('Location: index');
            }
        }
        ?>
        <html>
        <head><title>
        Add Article
        </title>
        <link rel="stylesheet" href="../assets/style.css"/>
        </head>
        <body>
            <div class="container">
                <a href="index.php" id="logo">CMS</a>
                <br/>
                <ol>
                    <h4>Add Article</h4>

                    <form action="add.php" method="post">
                        <input type="text" name="title" placeholder="Title" /> <br/><br/>
                        <textarea rows="15" cols="50" placeholder="Content" name="content"></textarea> <br/><br/>
                        <input type="submit" value="Add Article" />

                    </form>
                </ol>
                    
            </div>
        </body>
    </html>
    <?php
    }else{
        header("Location: index.php");
    }
?>